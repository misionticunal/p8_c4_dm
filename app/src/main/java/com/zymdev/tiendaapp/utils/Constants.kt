package com.zymdev.tiendaapp.utils

class Constants {

    companion object {
        val COMMNENTS_COLLECTION = "comments"
        val USERS_COLLECTION = "users"
        val PRODUCTS_COLLECTION = "products"
    }
}