package com.zymdev.tiendaapp

import android.os.Bundle
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.zymdev.tiendaapp.R
import com.zymdev.tiendaapp.databinding.ActivityHomeBinding

private lateinit var binding: ActivityHomeBinding

class HomeActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val navHost: BottomNavigationView = binding.bottomView

        val navController = findNavController(R.id.nav_host_home)

        val appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.navigation_fragment_home, R.id.navigation_fragment_orders, R.id.navigation_fragment_profile, R.id.navigation_fragment_products
            )
        )

        setupActionBarWithNavController(navController, appBarConfiguration)
        navHost.setupWithNavController(navController)
    }
}