package com.zymdev.tiendaapp.components.comments

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query


@Dao
interface CommentDao {
    @Query("SELECT * FROM Comments")
    suspend fun getAllComments(): List<CommentModel>

    @Query("SELECT * FROM Comments WHERE id = :commentId")
    suspend fun getCommentById(commentId: String): List<CommentModel>

    @Insert
    suspend fun insertComment(comment: CommentModel)

    @Insert
    suspend fun insertComments(comments: List<CommentModel>)


}