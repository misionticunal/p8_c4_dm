package com.zymdev.tiendaapp.components.comments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.zymdev.tiendaapp.R
import com.zymdev.tiendaapp.databinding.FragmentCommentsBinding

class CommentsFragment : Fragment() {

    private val commentViewModel: CommentViewModel by viewModels()
    private lateinit var _binding: FragmentCommentsBinding
    private val binding get() = _binding

    private lateinit var commentAdapter: CommentAdapter
    private lateinit var commentManager: LinearLayoutManager


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment

        _binding = FragmentCommentsBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onStart() {
        super.onStart()


        binding.commentsRefresh.setOnRefreshListener {
            commentViewModel.loadComments()
        }

        commentAdapter = CommentAdapter(
            listOf()
        )

        commentManager = LinearLayoutManager(requireContext())


        binding.commentList.apply {
            adapter = commentAdapter
            layoutManager = commentManager
        }


        commentViewModel.loadComments()

        commentViewModel.comments.observe(viewLifecycleOwner, Observer { comments ->
            binding.commentsRefresh.isRefreshing = false
            commentAdapter.updateItems(comments)
        })
    }
}