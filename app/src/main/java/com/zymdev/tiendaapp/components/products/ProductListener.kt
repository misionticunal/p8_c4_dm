package com.zymdev.tiendaapp.components.products

interface ProductListener {
    fun onClick(product: ProductModel)
}