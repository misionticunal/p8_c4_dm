package com.zymdev.tiendaapp.components.products

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.zymdev.tiendaapp.databinding.ItemProductBinding


class ProductAdapter(private var items: List<ProductModel>) : RecyclerView.Adapter<ProductAdapter.ItemProductHolder>(){

    class ItemProductHolder (val view: ItemProductBinding) : RecyclerView.ViewHolder(view.root)

    lateinit var listener: ProductListener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemProductHolder {
        return ItemProductHolder(
            ItemProductBinding.inflate(LayoutInflater.from(parent.context),parent, false )
        )
    }

    override fun onBindViewHolder(holder: ItemProductHolder, position: Int) {
        val product = items[position]

        holder.view.productItemName.text = product.name
        holder.view.productItemPrice.text = "\$ ${product.price.toString()}"
        holder.view.productItemDescription.text = "${product.description?.subSequence(0, 30)}..."

        Glide.with(holder.itemView).load(product.image).into(holder.view.productItemIcon)

        holder.view.productItemConstraintLayout.setOnClickListener {
            listener.onClick(product)
        }

    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun updateItems(products: List<ProductModel>){
        items = products
        notifyDataSetChanged()
    }


}