package com.zymdev.tiendaapp.components.signup

import android.content.Intent
import android.os.Bundle
import android.util.Patterns
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.auth.ktx.userProfileChangeRequest
import com.google.firebase.ktx.Firebase
import com.zymdev.tiendaapp.HomeActivity
import com.zymdev.tiendaapp.R
import com.zymdev.tiendaapp.databinding.FragmentSignUpBinding
import kotlinx.coroutines.tasks.await


/**
 * A simple [Fragment] subclass.
 * Use the [SignUpFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class SignUpFragment : Fragment() {
    // TODO: Rename and change types of parameters

    private lateinit var _binding: FragmentSignUpBinding
    private val binding get() = _binding

    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentSignUpBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onStart() {
        super.onStart()

        auth = Firebase.auth

        binding.loginButtonSignUp.setOnClickListener {
            var isValid = true

            if (binding.loginPasswordField.text.toString() != binding.loginConfirmPasswordField.text.toString()){
                isValid = false
                binding.loginConfirmPasswordLayout.error == "La contraña no coincide"
                binding.loginPasswordField.error == "La contraña no coincide"
                Toast.makeText(requireContext(), "La contraña no coincide",
                    Toast.LENGTH_SHORT).show()

            }else{
                binding.loginConfirmPasswordLayout.error == null
                binding.loginPasswordField.error == null
            }

            if (binding.loginPasswordField.text.toString().length < 6){
                binding.loginConfirmPasswordLayout.error == "El tamaño de la contraseña no es válido"
                Toast.makeText(requireContext(), "El tamaño de la contraseña no es válido",
                    Toast.LENGTH_SHORT).show()
            }else{
                binding.loginConfirmPasswordLayout.error == null
            }

            if (! (!binding.loginEmailField.text.isNullOrEmpty() && Patterns.EMAIL_ADDRESS.matcher(binding.loginEmailField.text.toString()).matches())){
                binding.loginEmailLayout.error == "Correo Inválido"
                Toast.makeText(requireContext(), "Correo Inválido",
                    Toast.LENGTH_SHORT).show()
            }else{
                binding.loginEmailLayout.error == null
            }

            if (binding.loginDisplayNameField.text.isNullOrEmpty() || binding.loginDisplayNameField.text.toString().length > 5){
                binding.loginDisplayNameLayout.error == "Nombre inválido"
                Toast.makeText(requireContext(), "Nombre inválido",
                    Toast.LENGTH_SHORT).show()
            }else{
                binding.loginDisplayNameLayout.error == null
            }

            if (isValid){
                auth.createUserWithEmailAndPassword(binding.loginEmailField.text.toString(), binding.loginPasswordField.text.toString()).addOnSuccessListener {
                    userProfileChangeRequest {
                        displayName = binding.loginDisplayNameField.text.toString()
                    }

                    val intent = Intent(activity, HomeActivity::class.java)
                    startActivity(intent)
                }
            }

        }
    }
}