package com.zymdev.tiendaapp.components.products

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.zymdev.tiendaapp.R
import com.zymdev.tiendaapp.databinding.FragmentProductDetailBinding


/**
 * A simple [Fragment] subclass.
 * Use the [ProductDetailFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ProductDetailFragment : Fragment() {

    private lateinit var _binding : FragmentProductDetailBinding
    private val binding get() = _binding

    private val productViewModel: ProductViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentProductDetailBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onStart() {
        super.onStart()

        productViewModel.selectedProduct.observe(viewLifecycleOwner, Observer { product ->
            binding.productItemName.text = product.name
            binding.productItemPrice.text = "\$ ${product.price.toString()}"
            binding.productItemDescription.text = product.description
        })
    }
}