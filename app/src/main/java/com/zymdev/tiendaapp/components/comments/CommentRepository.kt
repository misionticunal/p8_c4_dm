package com.zymdev.tiendaapp.components.comments

import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.zymdev.tiendaapp.utils.Constants
import kotlinx.coroutines.tasks.await

class CommentRepository() {
    private val db = Firebase.firestore

    suspend fun loadComments(): List<CommentModel> {

        val snapshot = db.collection(Constants.COMMNENTS_COLLECTION).get().await()
        return snapshot.toObjects(CommentModel::class.java)
    }

    suspend fun insertComments(comments: List<CommentModel>){
        db.collection(Constants.COMMNENTS_COLLECTION).add(comments).await()
    }

    suspend fun insertComment(comment: CommentModel): String {

        val documentReference = db.collection(Constants.COMMNENTS_COLLECTION).add(comment).await()

        return documentReference.id

    }

}