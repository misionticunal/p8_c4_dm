package com.zymdev.tiendaapp.components.products

import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

import com.zymdev.tiendaapp.utils.Constants
import kotlinx.coroutines.tasks.await

class ProductRepository {

    private val db = Firebase.firestore

    suspend fun loadProducts(): List<ProductModel> {

        val snapshot = db.collection(Constants.PRODUCTS_COLLECTION).get().await()
        return snapshot.toObjects(ProductModel::class.java)
    }

    suspend fun insertProducts(products: List<ProductModel>){
        db.collection(Constants.PRODUCTS_COLLECTION).add(products).await()
    }

    suspend fun insertProduct(products: ProductModel): String {

        val documentReference = db.collection(Constants.PRODUCTS_COLLECTION).add(products).await()

        return documentReference.id

    }
}