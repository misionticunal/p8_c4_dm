package com.zymdev.tiendaapp.components.comments

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Comments")
data class CommentModel(
    @PrimaryKey var id: Int? = null,
    var profileImage: String? = null,
    var name: String? = null,
    var message: String? = null


)