package com.zymdev.tiendaapp.components.login

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthException
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException
import com.google.firebase.auth.FirebaseAuthInvalidUserException
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.zymdev.tiendaapp.HomeActivity
import com.zymdev.tiendaapp.R
import com.zymdev.tiendaapp.databinding.FragmentLoginBinding

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class LoginFragment : Fragment() {

    private var _binding: FragmentLoginBinding? = null

    private lateinit var auth: FirebaseAuth

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        auth = Firebase.auth



        binding.loginButtonSignUp.setOnClickListener {
            findNavController().navigate(R.id.action_fragment_login_to_fragment_sign_up)
        }

        binding.loginButtonSignIn.setOnClickListener {

            auth.signInWithEmailAndPassword(
                binding.loginEmailField.text.toString(),
                binding.loginPasswordField.text.toString())
                .addOnCompleteListener(requireActivity()) { task ->
                    if (task.isSuccessful){
                        val intent = Intent(activity, HomeActivity::class.java)
                        startActivity(intent)
                    } else{
                        when (task.exception){
                            is FirebaseAuthInvalidCredentialsException -> {
                                Toast.makeText(requireContext(), "Contraseña incorrecta",
                                    Toast.LENGTH_SHORT).show()
                            }
                            is FirebaseAuthInvalidUserException -> {
                                Toast.makeText(requireContext(), "Usuario no existe",
                                    Toast.LENGTH_SHORT).show()
                            }
                            is FirebaseAuthException -> {
                                Toast.makeText(requireContext(), "Algo ha ocurrido",
                                    Toast.LENGTH_SHORT).show()
                            }

                        }
                    }
                }
        }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}