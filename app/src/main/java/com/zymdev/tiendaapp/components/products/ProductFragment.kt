package com.zymdev.tiendaapp.components.products

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.zymdev.tiendaapp.R
import com.zymdev.tiendaapp.databinding.FragmentProductBinding


/**
 * A simple [Fragment] subclass.
 * Use the [ProductFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ProductFragment : Fragment() {

    private val productViewModel: ProductViewModel by viewModels()

    private lateinit var _binding: FragmentProductBinding
    private val binding get() = _binding

    private lateinit var productAdapter: ProductAdapter
    private lateinit var productManager: GridLayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentProductBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onStart() {
        super.onStart()



        productAdapter = ProductAdapter(listOf())

        productManager = GridLayoutManager(requireContext(), 2)

        binding.productsRefresh.setOnRefreshListener {
            productViewModel.loadProducts()
        }

        binding.productList.apply {
            adapter = productAdapter
            layoutManager = productManager
        }

        productViewModel.products.observe(viewLifecycleOwner, Observer { products ->
            binding.productsRefresh.isRefreshing = false
            productAdapter.updateItems(products)
        })


        productAdapter.listener = object : ProductListener {
            override fun onClick(product: ProductModel) {
                productViewModel.selectProduct(product)
                findNavController().navigate(R.id.action_fragment_products_to_fragment_product_detail)
            }

        }

        productViewModel.loadProducts()
    }
}