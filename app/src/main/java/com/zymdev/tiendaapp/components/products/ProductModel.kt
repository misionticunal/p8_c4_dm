package com.zymdev.tiendaapp.components.products

import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity
class ProductModel (
    @PrimaryKey
    var id: String? = null,
    var name: String? = null,
    var price: Int? = null,
    var description: String? = null,
    var image: String? = null
)