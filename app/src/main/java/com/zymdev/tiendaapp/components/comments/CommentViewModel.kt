package com.zymdev.tiendaapp.components.comments

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch

class CommentViewModel : ViewModel() {

    private var _comments: MutableLiveData<List<CommentModel>> = MutableLiveData()
    val comments: LiveData<List<CommentModel>> get() = _comments

    private val commentRepository: CommentRepository = CommentRepository()

    fun loadComments(){
        viewModelScope.launch {
            _comments.postValue(commentRepository.loadComments())
        }
    }

}