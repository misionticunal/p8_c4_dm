package com.zymdev.tiendaapp.components.comments

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.zymdev.tiendaapp.databinding.ItemCommentsBinding

class CommentAdapter(private var items: List<CommentModel>) : RecyclerView.Adapter<CommentAdapter.ItemCommentHolder>() {

    class ItemCommentHolder (val view: ItemCommentsBinding) : RecyclerView.ViewHolder(view.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemCommentHolder {
        return ItemCommentHolder(
            ItemCommentsBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: ItemCommentHolder, position: Int) {
        val comment = items[position]
        holder.view.commentItemTitle.text = comment.name
        holder.view.commentItemSubtitle.text = comment.message

        /** TODO Add support to Firebase storage
         * */

        holder.view.root.setOnClickListener{
            TODO("Add click listener")
        }

    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun updateItems(comments: List<CommentModel>){
        items = comments
        notifyDataSetChanged()
    }


}