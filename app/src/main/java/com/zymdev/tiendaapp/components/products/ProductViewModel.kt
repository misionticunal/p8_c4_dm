package com.zymdev.tiendaapp.components.products

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch

class ProductViewModel : ViewModel() {
    private var _products: MutableLiveData<List<ProductModel>> = MutableLiveData()
    val products: LiveData<List<ProductModel>> get() = _products

    val _selectedProduct: MutableLiveData<ProductModel> = MutableLiveData()
    val selectedProduct: LiveData<ProductModel> get() = _selectedProduct

    private val productRepository: ProductRepository = ProductRepository()

    fun loadProducts(){
        viewModelScope.launch {
            _products.postValue(productRepository.loadProducts())
        }
    }

    fun selectProduct(product: ProductModel){
        _selectedProduct.postValue(product)
    }
}